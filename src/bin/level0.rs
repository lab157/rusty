use std::error::Error;
use std::fs::File;
use std::io::{BufRead, BufReader, ErrorKind};
use std::sync::mpsc::channel;
use std::time::{Duration, Instant};
use std::{env, thread};

fn main() -> Result<(), Box<dyn Error>> {
    let url_path: String = env::args()
        .nth(1)
        .ok_or_else(|| std::io::Error::new(ErrorKind::NotFound, "File name missing"))?;
    println!("Loading urls from {}", url_path);

    let start = Instant::now();
    let mut totals = Stats::new();
    let urls = read_urls(url_path)?;

    let mut threads = Vec::new();

    let receiver = {
        let (sender, receiver) = channel();
        for url in urls {
            let sender = sender.clone();
            threads.push(thread::spawn(
                move || -> Result<_, Box<dyn Error + Send + Sync>> {
                    let client = reqwest::blocking::Client::new();
                    let stats = get(&client, &url).unwrap();
                    println!(
                        "{} -> {:?} ({:.2} bytes/sec)",
                        url,
                        stats,
                        stats.bytes_per_sec().unwrap_or_default(),
                    );
                    sender.send(stats).unwrap();
                    Ok(())
                },
            ));
        }
        receiver
    };

    for thread in threads {
        thread.join().unwrap().ok();
    }

    while let Ok(stats) = receiver.recv() {
        totals.aggregate(&stats);
    }

    println!(
        "total {:?} ({:.2} bytes/sec)",
        totals,
        totals.bytes_per_sec().unwrap_or_default()
    );

    println!("wall clock time: {:?}", start.elapsed());
    Ok(())
}

fn read_urls(url_path: String) -> Result<Vec<String>, std::io::Error> {
    let url_file = File::open(url_path)?;
    let url_file = BufReader::new(url_file);
    url_file.lines().collect()
}

#[derive(Clone, Debug, PartialEq)]
struct Stats {
    elapsed_time: Duration,
    content_length: usize,
}

impl Stats {
    fn new() -> Self {
        Self {
            elapsed_time: Duration::default(),
            content_length: 0,
        }
    }

    fn aggregate(&mut self, other: &Stats) {
        self.elapsed_time += other.elapsed_time;
        self.content_length += other.content_length;
    }

    fn bytes_per_sec(&self) -> Option<f64> {
        match self.elapsed_time.as_secs_f64() {
            t if t >= 0.001 => Some(self.content_length as f64 / t),
            _ => None,
        }
    }
}

fn get(client: &reqwest::blocking::Client, url: &str) -> Result<Stats, Box<dyn Error>> {
    let now = Instant::now();
    let resp = client.get(url).send()?;
    println!("{}", resp.status());
    Ok(Stats {
        elapsed_time: now.elapsed(),
        content_length: resp.text()?.len(),
    })
}

trait FixBoxError<T> {
    fn fix_box(self) -> Result<T, Box<dyn Error + Send + Sync>>;
}

impl<T> FixBoxError<T> for Result<T, Box<dyn Error>> {
    fn fix_box(self) -> Result<T, Box<dyn Error + Send + Sync>> {
        match self {
            Err(err) => Err(err.to_string().into()),
            Ok(t) => Ok(t),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::Stats;
    use std::time::Duration;

    #[test]
    fn test_stats_aggregate() {
        let mut stats = Stats {
            elapsed_time: Duration::from_millis(500),
            content_length: 1000,
        };

        stats.aggregate(&Stats {
            elapsed_time: Duration::from_millis(1500),
            content_length: 3000,
        });

        assert_eq!(
            stats,
            Stats {
                elapsed_time: Duration::from_millis(2000),
                content_length: 4000,
            }
        );
    }

    #[test]
    fn test_stats_aggregate_empty() {
        let mut stats = Stats {
            elapsed_time: Duration::from_millis(500),
            content_length: 1000,
        };

        let stats2 = stats.clone();
        stats.aggregate(&Stats::new());

        assert_eq!(stats, stats2);
    }

    #[test]
    fn test_stats_bytes_per_sec() {
        let stats = Stats {
            elapsed_time: Duration::from_millis(500),
            content_length: 1000,
        };

        assert_eq!(stats.bytes_per_sec(), Some(2000f64))
    }

    #[test]
    fn test_stats_bytes_per_sec_empty() {
        let stats = Stats::new();

        assert_eq!(stats.bytes_per_sec(), None)
    }
}
